![](radar.png)

# Team RadAR: Radiotherapy Positioning using Augmented Reality
During our senior year of undergraduate school at UC Davis, our team of four members collaborated with our client, Varian Medical Systems, to develop a new method of streamlining patient positioning for radiation therapy.

# Table of Contents
- [Background](#markdown-header-background)
- [Our Solution](#markdown-header-our-solution)
- [Technology Used](#markdown-header-technology-used)
- [RadAR Team Members](#markdown-header-radar-team-members)
- [Acknowledgements](#markdown-header-acknowledgements)


# Background
We shadowed radiation therapists at the UC Davis Cancer Center to get a deeper understanding of the clinical need. There, patients are given 15 minute appointment slots, five days a week, for several weeks of treatment. In each of those 15 minute appointments, the majority of the time is dedicated to positioning the patient accurately on the treatment bed by using immobilization devices to hold the patient in place, and by aligning multiple room lasers to radiopaque tattoo markers on the patient. There are also surface monitoring cameras that display details about the patient's position on computer screens, which require radiation therapists to look back and forth between the patient and the screens to align the patient. Once this cumbersome process of positioning is completed, the therapists can then start treatment by sending radiation beams from the linear accelerator to the patient.

This is crucial, as being inaccurately positioned can lead to radiation damaging healthy tissue in the patient. Therefore, our needs statement is:     
**"There is a need for a more efficient way for radiation therapists to optimize the patient setup process."**


# Our Solution
Our method is to utilize Augmented Reality (AR) to display a holographic image of where the patient should be positioned. All the radiation therapist has to do is look at the holographic image that is overlaid on the patient, then position the physical patient right into the hologram. This makes it easier on the therapist, as he or she does not need to continuously look between the patient and a computer screen to estimate the position accuracy. The idea is that the easier it is to use, the faster the set up process could be.

As a plus, another feature is to make the AR headset recognize a barcode corresponding to patient information, and a panel of information will show up on a wall of the room. This provides easy access to information, thus reducing the burden of having to walk across the room to a computer screen in order to look at information, such as what specific immobilization devices a patient needs.

The new treatment workflow with our method then becomes:

1. **Using the headset, the therapist scans the patient's barcode (on an ID card), then scans a Quick Response (QR) code on the wall, so the software knows to display the patient's information at that spot.**
![](treatment1.png)

2. **The therapist walks to the treatment bed and scans any of the QR codes on the bed. Using that code as a reference point, the software displays the patient's holographic image at a specified location on the bed.**
![](treatment2.png)

3. **The therapist aligns the physical patient with the holographic image for positioning.**
![](treatment3.png)
![](treatment4.png)

Once we implemented our solution, we went back to the UC Davis Cancer Center to give a demo and we got amazing feedback on the possibilities of this idea.
![](therapist.jpg)


# Technology Used
- `Unity` = the software for creating holographic images and positioning holographic models in space
![](Unity.png)

- `Vuforia Toolkit` = a toolkit used in Unity for QR code and image recognition
- `Microsoft HoloLens 1` = the AR headset. Microsoft also developed Unity, so using this headset matched it with the AR software
- `Microsoft Visual Studio` = for deployment of the Holograms to the HoloLens
- `3D Slicer` = for converting a set of CT scans into a 3D model
![](slicer.png)


# RadAR Team Members
Priscilla Chan     
Yuqing (Hailey) Huang    
Janice Leung    
Laura Oelsner


# Acknowledgements
From Varian Medical Systems:   
- Sevelin Stanchev    
- Dr. John van Heteren     

From UC Davis Medical Center:   
- Dr. Robin Stern   
- Dr. Sonja Dieterich   
- Dr. Danie Hernandez   
- Dr. David Campos   

From UC Davis:
- Dr. Jennifer Choi   
- Robert Gresham   
- Alireza Tafazzol   
- Michael Sun   